import { Component, Output,EventEmitter } from '@angular/core';
import { signupUser } from '../models/user.interface';
import { HttpService } from '../services/http.service';
import { CommonService } from '../services/common.service';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'signup-popup',
  templateUrl: './signup.component.html',
  providers: [HttpService] 
})

export class SignupComponent {

@Output()
setUserDashboard : EventEmitter<any> = new EventEmitter<any>();

apiResponse : any;
errorMessage = "";
user =  new signupUser('', '', '', '', '');
passwordIndicator = false;
constructor(private httpService: HttpService, private commonService: CommonService){}


 //signup
signup(model: signupUser, isValid: boolean) {
  if(isValid)
  {
     if(model.password === model.cnfrmpassword)
      {
          this.passwordIndicator = false;
          this.commonService.showLoader();
          var signupOperation =  this.httpService.signup(model);
          signupOperation.subscribe(
          response => {
            this.apiResponse = response;
            if(this.apiResponse.message == "The email has already been taken.")
            {
               this.commonService.hideLoader();
               this.errorMessage = "This email has already been taken.";
            }
            else
            {
             
              this.setUserDashboard.emit(response);
              this.commonService.hideLoader();
              this.user = new signupUser('', '', '', '', '');
              this.closeSignupPopup ();
            }
          },
          err => {
            // Log errors if any error occured.
            console.log(err);
          });
      }
      else{
        this.passwordIndicator = true;
      }
      
  }
    else
    {
        this.errorMessage = "Please enter required details.";
    }
  }

closeSignupPopup ()
{
  this.errorMessage = "";
  this.user =  new signupUser('', '', '', '', '');
  $('#SignupModal').modal('hide'); 
}

openLoginPopup()
{
  this.errorMessage = "";
  $('#SignupModal').modal('hide'); 
  $('#LoginModal').modal('show'); 
}
}