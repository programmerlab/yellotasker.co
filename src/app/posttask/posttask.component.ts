import { Component, Output, EventEmitter , OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators , FormControl, NgForm  } from '@angular/forms';
import { Task } from '../models/posttask.interface';
import { HttpService } from '../services/http.service';
import { CommonService } from '../services/common.service';
import { User } from '../posttask/user';
import { DatePipe } from '@angular/common';
import { DatepickerModule } from 'angular2-material-datepicker'
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'posttask-popup',
  templateUrl: './posttask.component.html',
  providers: [HttpService, CommonService, DatePipe] 
})

export class PostTaskComponent implements OnInit {
postTaskStep1Form: FormGroup;
postTaskStep2Form: FormGroup;
postTaskStep3Form : FormGroup;
public charsLeft: number = 5000;
minDate = new Date(2000, 0, 1);
maxDate = new Date(2020, 0, 1);
 

@Output()
setUserDashboard : EventEmitter<any> = new EventEmitter<any>();
private apiResponse : any;
minLenIndicator = false;
errorIndicator = false;
detailsIndicator = true;
locationIndicator = false;
isStep1Visited = false;
isStep2Visited = false;
budgetIndicator = false;
taskFinishIndicator = false;
errorMessage = "";
task =  new Task('', '', '', '', '', '', 0, '', '', '', 0, 0, 0, '');
user : any;

constructor(private httpService: HttpService, private commonService: CommonService, private fb: FormBuilder
, private datePipe: DatePipe){
 
}

ngOnInit() {
    this.errorIndicator = false;
    this.detailsIndicator = true;
    this.locationIndicator = false;
    this.isStep1Visited = false;
    this.isStep2Visited = false;
    this.budgetIndicator = false;
    this.taskFinishIndicator = false;
    this.minLenIndicator = false;
    this.errorMessage = "";
    this.task =  new Task('', '', '', '', '', '', 0, '', '', '', 0, 0, 0, '');
    this.postTaskStep1Form = this.fb.group({
    title: ['', Validators.required],
    description: ['', [Validators.required, Validators.maxLength(5000), Validators.minLength(25)]]
  });
}

changed(description) {
    this.charsLeft = 5000 - description.length;
}

 //step 1 continue
onStep1Submit(form : any , model: any, isValid: boolean) {
  if(isValid)
  {
     this.task.title = model.title;
     this.task.description = model.description;
     this.isStep1Visited = true;
     this.showLocationTab();
     this.user = new User() ; 
   /*  this.postTaskStep2Form = this.fb.group({
        locationType : ['', Validators.required],
        address : [''],
        zipcode: [''],
        dueDate : [''],
        dueDateType : ['', Validators.required]
     });*/
      this.errorIndicator = false;
      this.minLenIndicator = false;
  }
  else
    {
      if(form.controls.description._errors.minlength)
      {
        this.minLenIndicator = true;
        this.errorIndicator = false;
      }
      else
       this.errorIndicator = true;
    }
  }
  
  getDate(dueDateType : string )
  {
     if(dueDateType == 'Today')
    {
       var date = new Date;
       this.user.dueDate = date;
    }
    else  if(dueDateType == 'Tommorow')
      {
        var date = new Date();
        date.setDate(date.getDate() + 1);
        this.user.dueDate = date;
      }
    else if(dueDateType == 'Within 1 week')
        {
          var date = new Date();
          date.setDate(date.getDate() + 7);
          this.user.dueDate = date;
        }
    else if(dueDateType == 'By a certain date')
      {
         this.user.dueDate = new Date;
      }
  }
  onStep2Submit(model: any, isValid: boolean) {
  if(isValid)
  {
     this.task.locationType = model.locationType;
     this.task.address = model.address;
     this.task.zipcode = model.zipcode;
    // this.task.due_date = model.dueDate;
     this.task.dueDate = this.user.dueDate ;
     this.task.dueDateType = this.user.dueDateType ;
     this.isStep2Visited = true;
     this.showBudgetTab();
     this.postTaskStep3Form = this.fb.group({
        peopleRequired : ['', [Validators.required, Validators.minLength(1)]],
       // typeOfPeople : ['', []],
        budgetType : ['', [Validators.required]],
        amount : ['', [Validators.required]],
        hourlyRate : ['']
     });
  }
  else
    {
      this.errorIndicator = true;
    }
  }

  onStep3Submit(model: any, isValid: boolean) {
  if(isValid)
  {
     this.task.people_required = model.peopleRequired;
     this.task.typeOfPeople = model.typeOfPeople;
     this.task.budgetType = model.budgetType;
     if(this.task.budgetType == 'Total')
     {
      this.task.totalAmount = model.amount;
     }
     else
     {
       this.task.hourlyRate = +model.hourlyRate;
       this.task.totalHours = +model.amount;
       this.task.totalAmount = this.task.hourlyRate * this.task.totalHours;
     }
     this.task.userId = this.commonService.getCookieValues("userid");
          this.commonService.showLoader();
          var postTaskOperation =  this.httpService.posttask(this.task);
          postTaskOperation.subscribe(
          response => {
            this.apiResponse = response;
            if(this.apiResponse.message !== "Task  created successfully.")
            {
               this.commonService.hideLoader();
               this.errorMessage = "Something went wrong. Please try after some time. ";
            }
            else
            {
              this.commonService.hideLoader();
              this.task =  new Task('', '', '', '', '', '', 0, '', '', '', 0, 0, 0, '');
              this.detailsIndicator = false;
              this.locationIndicator = false;
              this.budgetIndicator = false;
              this.taskFinishIndicator = true;
              setTimeout(()=>{ 
                this.closePosttaskPopup();
              }, 3000);
            }
          },
          err => {
            // Log errors if any error occured.
            console.log(err);
          });
  }
  else
    {
      this.errorIndicator = true;
    }
  }

closePosttaskPopup ()
{
  this.ngOnInit();
  $('#PostTaskModal').modal('hide'); 
}

showDetailsTab()
{
    this.detailsIndicator = true;
    this.locationIndicator = false;
    this.budgetIndicator = false;
    this.taskFinishIndicator = false;
}

showLocationTab()
{
   if(this.isStep1Visited == true)
   {
      this.detailsIndicator = false;
      this.locationIndicator = true;
      this.budgetIndicator = false;
      this.taskFinishIndicator = false;
   }
}

showBudgetTab()
{
  if(this.isStep1Visited == true && this.isStep2Visited == true)
  {
    this.detailsIndicator = false;
    this.locationIndicator = false;
    this.budgetIndicator = true;
    this.taskFinishIndicator = false;
  }
}

}