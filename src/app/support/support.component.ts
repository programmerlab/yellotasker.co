import { Component , OnInit } from '@angular/core';

@Component({
  selector: 'routing-root',
  templateUrl: './support.component.html'
})
export class SupportComponent  implements OnInit {
  ngOnInit() {
    window.scrollTo(0,0);
  }
}
