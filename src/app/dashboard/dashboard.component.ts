import { Component, OnInit } from '@angular/core';
import { Injector } from '@angular/core';
import {AppComponent} from '../app.component';
import { CommonService } from '../services/common.service';
import { HttpService } from '../services/http.service';
@Component({
  selector: 'routing-root',
  templateUrl: './dashboard.component.html',
  providers: [HttpService, CommonService] 
})
export class DashboardComponent implements OnInit{
    parentComponent : any;
     apiResponse : any;
     taskList : any ;
     categoryList :any;
   constructor(private inj:Injector, private httpService: HttpService, private commonService: CommonService){
        this.parentComponent = this.inj.get(AppComponent);
        
    }
    ngOnInit() {
     this.commonService.showLoader();
     this.httpService.getLatestPostedTask().subscribe(
      data => {
        this.apiResponse = data;
        if(this.apiResponse.message == 'List of tasks.')
        {
          this.taskList = this.apiResponse.data;
          this.commonService.hideLoader();
          console.log(this.taskList );
        }
    });
    this.httpService.getCategory().subscribe(
      data => {
        this.apiResponse = data;
        this.commonService.showLoader();
        if(this.apiResponse.message == 'Category dashboard list')
        {
          this.categoryList=this.apiResponse .data;
          this.commonService.hideLoader();
        }
        else{
        //   this.commentIndicator = false;
           this.commonService.hideLoader();
        }
    });
  }
    openPostTask()
    {
      this.parentComponent.openPostTaskPopup();
    }
}
