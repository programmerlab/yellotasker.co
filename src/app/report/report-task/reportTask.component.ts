import { Component , OnInit } from '@angular/core';
import { Injector } from '@angular/core';
import {AppComponent} from './../../app.component';
import { CommonService } from './../../services/common.service';
import { HttpService } from './../../services/http.service';

@Component({
  selector: 'reportTask-popup',
  templateUrl: './reportTask.component.html',
  providers: [HttpService, CommonService] 
})
export class ReporttaskComponent  implements OnInit {
  parentComponent:any;
  apiResponse : any;
  reasonList : any;
  constructor( private inj:Injector,private httpService: HttpService, private commonService: CommonService){
    this.parentComponent = this.inj.get(AppComponent);
  }
  ngOnInit() {
    window.scrollTo(0,0);
    this.httpService.getReason().subscribe(
      data => {
        this.apiResponse = data;
        this.commonService.showLoader();
        if(this.apiResponse.message == 'reason list')
        {
          this.reasonList=this.apiResponse.data.taskReason;       
          this.commonService.hideLoader();
        }
        else{
        //   this.commentIndicator = false;
           this.commonService.hideLoader();
        }
    });
  }

}
