import { Component , OnInit } from '@angular/core';

@Component({
  selector: 'routing-root',
  templateUrl: './myTask.component.html'
})
export class MyTaskComponent  implements OnInit {
  ngOnInit() {
    window.scrollTo(0,0);
  }
}