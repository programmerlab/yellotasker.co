//angular libraries 
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import { ReactiveFormsModule } from '@angular/forms'; 
import { DatepickerModule } from 'angular2-material-datepicker'
import { DatePipe } from '@angular/common';
import {TimeAgoPipe} from 'time-ago-pipe';
//application components 
import { AppComponent } from './app.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { BrowseTaskComponent } from './browsetask/browsetask.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { PostTaskComponent } from './posttask/posttask.component';
import { ProfileComponent } from './profile/profile.component';
import { TermsComponent } from './terms/terms.component';
import { CategoryListingComponent } from './category-listing/categoryListing.component';
import { CategoryDetailComponent } from './category-detail/categoryDetail.component';
import { AboutUsComponent } from './about-us/about.component';
import { HelpComponent } from './help/help.component';
import { ContactUsComponent } from './contact-us/contact.component';
import { SupportComponent } from './support/support.component';
import { ResetPasswordComponent } from './reset-password/reset.component'; // forget password pop up 
import { UpdatePasswordComponent } from './update-password/updatePassword.component'; // update password from reset link from mail 
import { ChangePasswordComponent } from './change-password/changePassword.component'; //change password by email 
import { MyTaskComponent } from './my-task/myTask.component';
import { ReporttaskComponent } from './report/report-task/reportTask.component';
import { PublicProfileComponent } from './public-profile/publicProfile.component';
const appRoutes: Routes = [
    { path: 'browse-task', component: BrowseTaskComponent },
    { path: 'category-detail', component: CategoryDetailComponent },
    { path: 'category-list', component: CategoryListingComponent },
    { path: 'settings', component: ProfileComponent },
    { path: 'profile', component: PublicProfileComponent },
    { path: 'contact-us', component: ContactUsComponent },
    { path: 'about-us', component: AboutUsComponent },
    { path: 'help', component: HelpComponent },
    { path: 'terms-and-conditions', component: TermsComponent },
    { path: 'support', component: SupportComponent },
    { path: 'update-password' , component: UpdatePasswordComponent },
    { path: 'change-password' , component: ChangePasswordComponent },
    { path: 'my-task' , component: MyTaskComponent },
    { path: '', component: DashboardComponent },
    { path: '**', redirectTo: '' }
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    BrowseTaskComponent,
    LoginComponent,
    SignupComponent,
    PostTaskComponent,
    ProfileComponent,
    TermsComponent,
    CategoryListingComponent,
    CategoryDetailComponent,
    AboutUsComponent,
    HelpComponent,
    ContactUsComponent,
    SupportComponent,
    ResetPasswordComponent,
    UpdatePasswordComponent,
    ChangePasswordComponent,
    TimeAgoPipe,
    MyTaskComponent,
    ReporttaskComponent,
    PublicProfileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes,{ useHash: true }),
    HttpModule, 
    ReactiveFormsModule,
    DatepickerModule 
  ],
  
  providers: [CookieService , DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
