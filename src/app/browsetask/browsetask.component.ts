import { Component , OnInit} from '@angular/core';
import { HttpService } from '../services/http.service';
import { CommonService } from '../services/common.service';
import {ReversePipe} from '../pipes/reversePipes';
import {Comment} from './comment';
import {Injector} from '@angular/core';
import {AppComponent} from '../app.component';


declare var jquery:any;
declare var $ :any;


@Component({
  selector: 'routing-root',
  templateUrl: './browsetask.component.html',
  providers: [HttpService, CommonService, ReversePipe]
})
export class BrowseTaskComponent implements OnInit{
  apiResponse : any;
  pageNum : number;
  taskList : any;
  itemDetail : any;
  comment: any;
  offerObject :any;
  commentList : object;
  taskListIndicator : boolean;
  commentIndicator : boolean;
  replyIndicator : boolean;
  parentComponent : any;
  anchorDisable : boolean;
  anchorReplyDisable : boolean;
  replyCommentDesc : string;
  repliedObjectList : any ;
  constructor(private inj:Injector,private httpService: HttpService, private commonService: CommonService,
  private reversePipe: ReversePipe){
    this.parentComponent = this.inj.get(AppComponent);
  }
  ngOnInit() {
    this.pageNum = 1;
    this.anchorDisable = true;
    this.anchorReplyDisable = true;
     this.commonService.showLoader();
     this.httpService.browseAllItems(this.pageNum).subscribe(
      data => {
        this.apiResponse = data;
        if(this.apiResponse.message == 'List of tasks.')
        {
          this.reversePipe.transform(this.apiResponse.data);
          this.taskList = this.apiResponse.data;
          this.taskListIndicator = true;
          this.commonService.hideLoader();
          console.log(this.taskList );
        }
    });

  }
  showTaskDescription(item)
  {
    var userid = this.commonService.getCookieValues("userid");
    if(!userid)
    this.parentComponent.openLoginPromptPopup();
    else{
        this.itemDetail = item;
        this.comment = new Comment();
        this.getAllComments(this.itemDetail.id);
        this.anchorDisable = true;
        this.anchorReplyDisable= true;
        $('#taskDescriptionModal').modal({backdrop: 'static', keyboard: false},'show');
    }
  }

  getAllComments(taskId)
  {
    this.httpService.getAllComment(taskId).subscribe(
      data => {
        this.apiResponse = data;
        this.commonService.showLoader();
        if(this.apiResponse.message == 'Comments list')
        {
          this.commentIndicator = true;
          this.commentList = this.apiResponse.data;
          this.commonService.hideLoader();
          console.log(this.commentList);
        }
        else{
           this.commentIndicator = false;
           this.commonService.hideLoader();
        }
    });
  }

  onReply(item)
  {
    $('#item_'+ item.id).show();
    this.anchorDisable = true;
  }

  replyComment(commentDesc : string, itemDetail : any, commentId)
  {
    this.comment = {};
    this.comment.commentDescription = commentDesc;
    this.comment.taskId = itemDetail.id;
    this.comment.userId = this.commonService.getCookieValues("userid");
    this.comment.commentId = commentId;
     this.httpService.replyComment(this.comment).subscribe(
      data => {
        this.apiResponse = data;
        if(this.apiResponse.message == 'Comment replied!')
        {

          this.replyCommentDesc = "";
          this.getAllComments(this.comment.taskId);
          this.comment = {};
        }
    });
  }

  getMoreItems()
  {
    this.pageNum = this.pageNum + 1;
    this.taskListIndicator = false;
    this.commonService.showLoader();
     this.httpService.browseAllItems(this.pageNum).subscribe(
      data => {
        this.apiResponse = data;
        if(this.apiResponse.message == 'List of tasks.')
        {
          this.reversePipe.transform(this.apiResponse.data);
          this.taskList = this.taskList.concat(this.apiResponse.data);
          this.taskListIndicator = true;
          this.commonService.hideLoader();
          console.log(this.taskList );
        }
    });
  }
  addComment(model : any, itemDetail : any)
  {
    if(model.commentDescription)
    {
      this.comment.commentDescription = model.commentDescription;
      this.comment.taskId = itemDetail.id;
      this.comment.userId = this.commonService.getCookieValues("userid");

      this.httpService.postComment(this.comment).subscribe(
        data => {
          this.apiResponse = data;
          if(this.apiResponse.message == 'Reply added successfully.')
          {
            //get comment list
            this.comment = {};
            this.getAllComments(itemDetail.id);
          }
      });
    }

  }

  addCommentChange(event)
  {
     if(this.comment.commentDescription && this.comment.commentDescription != "")
    {
      this.anchorDisable = false;
    }
    else
    {
       this.anchorDisable = true;
    }
  }

  addCommentReplyChange(event)
  {
     if(this.comment.commentDescription && this.comment.commentDescription != "")
    {
      this.anchorReplyDisable= false;
    }
    else
    {
       this.anchorReplyDisable= true;;
    }
  }

  checkIfReplyExists(id : any, List :any)
  {
    this.repliedObjectList = {};
    this.repliedObjectList = List.filter(x => x.commentId === id);
    if( this.repliedObjectList.length > 0 )
    {
      return true;
    }
    else{
      return false;
    }
  } 


  showOfferPopup()
  {
    console.log(this.itemDetail);

    this.offerObject={};
    this.offerObject.interestedUsreId = this.commonService.getCookieValues("userid");
    this.offerObject.taskId = this.itemDetail.id;
    this.offerObject.assignUserId = this.itemDetail.user_detail.id;
    this.httpService.offerPosting(this.offerObject).subscribe(
      data => {
        this.apiResponse = data;
        this.commonService.showLoader();
        if(this.apiResponse.message == 'Offer posted successfully.')
        {
          $('#OfferModalConfirmation').modal('hide');
          $('#OfferModalSuccess').modal('show');
          this.commonService.hideLoader();
          console.log(this.commentList);
        }
        else{
        //   this.commentIndicator = false;
           this.commonService.hideLoader();
        }
    });
  }

  showOfferConfirmationPopup(itemDetail)
  {
      this.itemDetail = itemDetail;
          var taskId = this.itemDetail.id;
          this.httpService.userList(taskId).subscribe(
            data => {
              this.apiResponse = data;
              this.commonService.showLoader();
              if(this.apiResponse.message == 'Offer posted successfully.')
              {
                $('#OfferModalConfirmation').modal('hide');
                $('#OfferModalSuccess').modal('show');
                this.commonService.hideLoader();
                console.log(this.commentList);
              }
              else{
              //   this.commentIndicator = false;
                 this.commonService.hideLoader();
              }
          });
     $('#OfferModalConfirmation').modal('show');
  }

  reportTask()
  {
     $('#taskDescriptionModal').modal('hide'); 
     $('#ReportTaskModal').modal({backdrop: 'static', keyboard: false},'show'); 
  }
}
