import { Component, Output, EventEmitter } from '@angular/core';
import { loginUser } from '../models/user.interface';
import { HttpService } from '../services/http.service';
import { CommonService } from '../services/common.service';
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'login-popup',
  templateUrl: './login.component.html',
  providers: [HttpService, CommonService] 
})

export class LoginComponent {

@Output()
setUserDashboard : EventEmitter<any> = new EventEmitter<any>();

//private apiResponse = {};
errorMessage = "";
user =  new loginUser('', '');
apiResponse : any;

constructor(private httpService: HttpService, private commonService: CommonService){}

 //login
login(model: loginUser, isValid: boolean) {
  this.errorMessage = "";
  if(isValid)
  {
      this.commonService.showLoader();
      var loginOperation =  this.httpService.login(model);
      loginOperation.subscribe(
        response => {
          this.apiResponse = response;
          if(this.apiResponse.message == 'Successfully logged in.' )
          {
           this.setUserDashboard.emit(response);
           this.commonService.hideLoader();
           this.user = new loginUser('', '');
           this.closeLoginPopup ();
          }
          else if(this.apiResponse.message == 'Invalid email or password. Try again!')
          {
              this.errorMessage = 'Invalid email or password. Try again!';
              this.commonService.hideLoader();
          }
          
        },
        err => {
          console.log(err);
         }
      );
  }
  else
    {
      if(model.email == "" || model.password == "")
      this.errorMessage = "Please enter required details.";
    }
  }

closeLoginPopup ()
{
  this.errorMessage = "";
  this.user =  new loginUser('', '');
  $('#LoginModal').modal('hide'); 
}

openForgetPopup()
{
   this.errorMessage = "";
   $('#LoginModal').modal('hide'); 
   $('#myModalreset').modal({backdrop: 'static', keyboard: false},'show'); 
}
openSignupPopup()
{
    this.errorMessage = "";
    $('#LoginModal').modal('hide'); 
   $('#SignupModal').modal('show'); 
}
}