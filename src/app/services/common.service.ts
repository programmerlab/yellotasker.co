import { Injectable }     from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

declare var jquery:any;
declare var $ :any;

@Injectable()
export class CommonService {

cookieResult: string;
constructor( private cookieService: CookieService ) { }

showLoader()
{
    //show Loader
    $('#loading').show();
}

hideLoader()
{
    //hide Loader
    $('#loading').hide();
}

setCookieValues(cookieName, cookieValue)
{
    this.cookieService.set( cookieName, cookieValue);
}

getCookieValues(cookieName)
{
   
    const cookieExists: boolean = this.cookieService.check(cookieName);
    if(cookieExists)
    this.cookieResult = this.cookieService.get(cookieName);
    else
     this.cookieResult = null;
    return this.cookieResult;
}

deleteCookieValues(cookieName)
{
    this.cookieService.delete(cookieName);
}

}