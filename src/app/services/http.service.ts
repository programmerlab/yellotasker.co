import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams  } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//import {sign} from '../models/user.interface';

@Injectable()
export class HttpService {
     // Resolve HTTP using the constructor
     constructor (private http: Http) {}
     // private instance variable to hold base url
     private API_ENDPOINT_UAT = 'http://api.yellotasker.co/api/v1/';

     login(user : any) : Observable<any[]>  {
        //let bodyString = JSON.stringify(user); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option

        // Parameters obj-
         let params: URLSearchParams = new URLSearchParams();
        params.set('email',  user.email);
        params.set('password', user.password);

        options.params = params;
        return this.http.get(this.API_ENDPOINT_UAT + 'user/login', options)
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                    console.log('error within catch is ' + Response)
                    if(error.status === 404)
                        return Observable.throw('not found error');

                    return Observable.throw('app error');
                    });

        }
      //set link to inbox
      forgetPassword(user : any) : Observable<any[]>  {
        //let bodyString = JSON.stringify(user); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option

        // Parameters obj-
         let params: URLSearchParams = new URLSearchParams();
        params.set('email',  user.email);

        options.params = params;
        return this.http.get(this.API_ENDPOINT_UAT + 'user/forgotPassword', options)
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                    console.log('error within catch is ' + Response)
                    if(error.status === 404)
                        return Observable.throw('not found error');

                    return Observable.throw('app error');
                    });
        }

      //change password after clicking link on reset password
     resetPassword(user : any) : Observable<any[]>  {
        let bodyString = JSON.stringify(user); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.API_ENDPOINT_UAT + 'password/reset', user , options)
                         .map((res:Response) =>  res.json())
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
        }

    //change password on by email
     changePassword(user : any) : Observable<any[]>  {
        let bodyString = JSON.stringify(user); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.API_ENDPOINT_UAT + 'user/updatePassword', user , options)
                         .map((res:Response) =>  res.json())
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
        }
     signup(user : Object) : Observable<any[]>
     {
        let bodyString = JSON.stringify(user); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.API_ENDPOINT_UAT + 'user/signup', user , options)
                         .map((res:Response) =>  res.json())
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
     }

     posttask(task : Object) : Observable<any[]>
     {
        let bodyString = JSON.stringify(task); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.API_ENDPOINT_UAT + 'postTask/createTask', task , options)
                         .map((res:Response) =>  res.json())
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
     }

      postComment(comment : Object) : Observable<any[]>
     {
        let bodyString = JSON.stringify(comment); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.API_ENDPOINT_UAT + 'comment/post', comment , options)
                         .map((res:Response) =>  res.json())
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
     }

     replyComment(comment : Object) : Observable<any[]>
     {
        let bodyString = JSON.stringify(comment); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.API_ENDPOINT_UAT + 'comment/post?commentReply=yes', comment , options)
                         .map((res:Response) =>  res.json())
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
     }

     browseAllItems(pageNum : number) : Observable<any[]> {
         // ...using get request
         return this.http.get(this.API_ENDPOINT_UAT + 'getPostTask?page_size=10&page_num=' + pageNum)
                        // ...and calling .json() on the response to return data
                         .map((res:Response) =>  res.json())
                         //...errors if any
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

     }
     getLatestPostedTask() : Observable<any[]> {
         return this.http.get(this.API_ENDPOINT_UAT + 'getPostTask?limit=4&taskStatus=open')
                        // ...and calling .json() on the response to return data
                         .map((res:Response) =>  res.json())
                         //...errors if any
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

     }
      getAllComment(taskId) : Observable<any[]> {
         return this.http.get(this.API_ENDPOINT_UAT + 'comment/post?getCommentBy=task&taskId='+ taskId)
                        // ...and calling .json() on the response to return data
                         .map((res:Response) =>  res.json())
                         //...errors if any
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

     }
       offerPosting(offerObject : Object) : Observable<any[]> {

         let bodyString = JSON.stringify(offerObject); // Stringify payload
         let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
         let options       = new RequestOptions({ headers: headers }); // Create a request option
              return this.http.post(this.API_ENDPOINT_UAT + 'makeOffer', offerObject , options)
                      .map((res:Response) =>  res.json())
                          .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

       }
       userList(taskId : Object) : Observable<any[]> {
        return this.http.get(this.API_ENDPOINT_UAT + 'taskOffer/' + taskId)
                      .map((res:Response) =>  res.json())
                          .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

       }
       getCategory() : Observable<any[]> {
        return this.http.get(this.API_ENDPOINT_UAT + 'categoryDashboard')
                      .map((res:Response) =>  res.json())
                          .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

       }
       getReason() : Observable<any[]> {
        return this.http.get(this.API_ENDPOINT_UAT + 'getReason')
                      .map((res:Response) =>  res.json())
                          .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

       }
    //    getReportByUserId() : Observable<any[]> {
    //     return this.http.get(this.API_ENDPOINT_UAT + 'getReport/user/'+ userId)
    //                   .map((res:Response) =>  res.json())
    //                       .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

    //    }
       getReportByTaskId() : Observable<any[]> {
        return this.http.get(this.API_ENDPOINT_UAT + 'categoryDashboard')
                      .map((res:Response) =>  res.json())
                          .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

       }
}
