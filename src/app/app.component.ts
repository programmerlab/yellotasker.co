import { Component , OnInit} from '@angular/core';
import { CommonService } from './services/common.service';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [CommonService] 
})

export class AppComponent implements OnInit {
errorMessage = "";
loginIndicator:  boolean= false;
loginDetails : any;

constructor(private commonService: CommonService){}

ngOnInit() {
   this.loginDetails = {};
   this.loginDetails.id  = this.commonService.getCookieValues("userid");
   if(this.loginDetails.id != null)
   {
        this.loginIndicator = true;
        this.loginDetails.first_name = this.commonService.getCookieValues("userFirstName");
        this.loginDetails.last_name = this.commonService.getCookieValues("userLastName");
   }
 }

setUserDashboard(loginDetails: any) {
    this.loginDetails = {};
    this.loginDetails = loginDetails.data;
    if(this.loginDetails.id)
    {
        this.loginIndicator = true;
        this.commonService.setCookieValues("userid", this.loginDetails.id);
        this.commonService.setCookieValues("userFirstName", this.loginDetails.first_name);
        this.commonService.setCookieValues("userLastName", this.loginDetails.last_name);
    }
}

openLoginPopup ()
{
  $('#loginPrompt').modal('hide'); 
  $('#LoginModal').modal({backdrop: 'static', keyboard: false},'show'); 
}

openSignupPopup ()
{
  $('#loginPrompt').modal('hide'); 
  $('#SignupModal').modal({backdrop: 'static', keyboard: false},'show'); 
}

openLoginPromptPopup ()
{
  $('#loginPrompt').modal({backdrop: 'static', keyboard: false},'show'); 
}

openPostTaskPopup()
{
  if(this.loginDetails != null && this.loginDetails.id != null)
    $('#PostTaskModal').modal({backdrop: 'static', keyboard: false},'show'); 
  else
    this.openLoginPromptPopup();
}
logout()
{
 this.loginIndicator = false;
 this.loginDetails = null;
 this.commonService.deleteCookieValues("userid");
 this.commonService.deleteCookieValues("userFirstName");
 this.commonService.deleteCookieValues("userLastName");
}

}
