import { Component , OnInit } from '@angular/core';

@Component({
  selector: 'routing-root',
  templateUrl: './publicProfile.component.html'
})
export class PublicProfileComponent  implements OnInit {

  ngOnInit() {
    window.scrollTo(0,0);
  }

}
